# Blahlysm

An automatic submit performance tool.

## Prerequisites

- JDK 8 or later

##How to build

`$ ./gradlew build docker`

## How to run

`$ docker run -v /path/to/application.properties:/application.properties jp.ewigkeit/blahlysm`

## Settings

- `blahlysm.loginId`

- `blahlysm.password`

- `blahlysm.performance` (choose: `Excellent`, `WellDone`, `NotSoGood`)

- `blahlysm.randomPerformance` (default: `false`)

